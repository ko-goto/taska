package taskA;

import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class Main {


	private final static int ACTIVATION_CONDITION_NUM = 5;

	public static void main(String[] args) {

        System.out.println("入力してください。");

        try(Scanner sc1 = new Scanner(System.in) ){

            ArrayList<Result> results = new ArrayList<Result>();

	        // セット数
	        int line1 = Integer.parseInt(sc1.nextLine());

	        for(int i= 0; i < line1; i++) {

	        	boolean result = true;

	        	// 必殺技の回数
	        	int line2 = Integer.parseInt(sc1.nextLine());
	        	// 敵グループの数
	        	 String line3 = sc1.nextLine();
	        	// 敵を倒した数
	        	int count = 0;
	        	// 使用回数
	        	int useCount = line2;

	        	String[] enemys = line3.split(" ", 0);

	        	for(String enemy : enemys) {

	        		if(StringUtils.isNotEmpty(enemy) && StringUtils.isNumeric(enemy)) {

	        			int num = Integer.parseInt(enemy);

	        			if(ACTIVATION_CONDITION_NUM < num) {
	                		if(useCount > 0) {
	                			useCount--;
	                		}else {
	                			result = false;
	                			break;
	                		}
	        			}
	        			// 数を足す
	        			count = count + num;
	        		}

	        	}

	        	results.add(new Result(result, count));

	        }

	        System.out.println("結果");
        	for(Result result : results) {
        		System.out.println(result);
        	}
        }
	}
}
