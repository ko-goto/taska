package taskA;

public class Result {

	private final boolean result;
	private final int count;


	public Result(boolean result, int count){
		this.result = result;
		this.count = count;

	}

	@Override
	public String toString() {

		String res = String.valueOf(this.result);

		return res.substring(0, 1).toUpperCase() + res.substring(1).toLowerCase() + " " + String.valueOf(this.count);
	}
}
